package udem.edu.co.ropero.almacen;
/*
@aoutor romario
@es el interfaz Ropa
@todos sus metodos son abstractas
*/
public interface Ropa {

	public String toString();
	
	public String usar();
}
