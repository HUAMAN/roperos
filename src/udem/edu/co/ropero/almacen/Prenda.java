package udem.edu.co.ropero.almacen;
//la clase prenda es abstract la cual no pude ser instanciadas.
public abstract class Prenda  implements Ropa{
        
	private String color;
	private String marca;
	private int precio;
	private int talla;
	
	
	public Prenda(String color, String marca, int precio, int talla) {
		super();
		this.color = color;
		this.marca = marca;
		this.precio = precio;
		this.talla = talla;
	}


	
	//getters and setters

	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public int getPrecio() {
		return precio;
	}


	public void setPrecio(int precio) {
		this.precio = precio;
	}


	public int getTalla() {
		return talla;
	}


	public void setTalla(int talla) {
		this.talla = talla;
	}
	
	
}
