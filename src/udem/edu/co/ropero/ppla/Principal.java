package udem.edu.co.ropero.ppla;

import java.util.ArrayList;

import udem.edu.co.ropero.Boxer;
import udem.edu.co.ropero.Calsetin;
import udem.edu.co.ropero.Jean;
import udem.edu.co.ropero.almacen.Ropa;

/*
*@autor ROMARIO QUISPE HUAMAN
*/
public class Principal {

	public static void main(String[] args) {
		
		ArrayList<Ropa> ropa = new ArrayList();
		
		
		ropa.add(new Jean("azul","diesel",200000,32,"jean"));
		ropa.add(new Calsetin("azul","diesel",200000,32, "tobillero"));
                ropa.add(new Boxer("azul", "diesel",20000,32,"cinta"));
		
		for(Ropa ropaunidad:ropa) {
			
			System.out.println(ropaunidad.toString());
		}
		
	}

}
